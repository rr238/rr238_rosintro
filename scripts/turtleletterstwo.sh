# !/ usr / bin / bash

rosservice call /spawn 2.5 5.0 0.0 "turtle2"
rosservice call /kill "turtle1"
rosservice call /turtle2/set_pen 0 255 255 2 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 3.0, 0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, -0.8, 0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 2.2, 0.0]' '[0.0,0.0,-3.2]'

rosservice call /kill "turtle2"
rosservice call /spawn 5.0 5.0 0.0 "turtle3"
rosservice call /turtle3/set_pen 255 255 0 2 0
rostopic pub -1 /turtle3/cmd_vel geometry_msgs/Twist -- '[0.0, 2.2, 0.0]' '[0.0,0.0,0.0]'
rosservice call /kill "turtle3"
rosservice call /spawn 5.0 8.0 0.0 "turtle4"
rosservice call /turtle4/set_pen 255 255 0 2 0
rostopic pub -1 /turtle4/cmd_vel geometry_msgs/Twist -- '[0.1,0.0,0.0]' '[0.0,0.0,3.6]'
rosservice call /kill "turtle4"
