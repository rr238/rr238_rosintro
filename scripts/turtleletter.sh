# !/ usr / bin / bash

rosservice call /spawn 2.5 5.0 0.0 "turtle2"
rosservice call /kill "turtle1"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 3.0, 0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, -0.8, 0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 2.2, 0.0]' '[0.0,0.0,-3.2]'

