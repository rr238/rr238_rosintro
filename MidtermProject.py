#imports
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
import numpy as np

class MidtermProject(object):
    """MidtermProject"""

    
    # This function is used for initialization
    def __init__(self):
        super(MidtermProject, self).__init__()

        #Initialize move it commander and rospy node
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("MidtermProject", anonymous=True)

        robot = moveit_commander.RobotCommander() #instantiate robot commander object
        scene = moveit_commander.PlanningSceneInterface() #initializing a remote interface

        #Instantiate a MoveGroup Commander Object
        group_name = "manipulator" 
        move_group = moveit_commander.MoveGroupCommander(group_name)

        #Display Trajectories in RViz
        display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,)
        
        self.move_group = move_group    
    
    
    # This function takes joint goals and converts the angles from degrees into radians
    # and then creates a list to easily input joint space coordinates
    # The elements of the list correspond to the following joints: Shoulder Pan, Shoulder Lift, Elbow,
    # Wrist 1, Wrist 2, and Wrist 3 
    def jointarray(self, goal_0, goal_1, goal_2, goal_3, goal_4, goal_5):
        joint_goal = [goal_0*(np.pi/180), goal_1*(np.pi/180), goal_2*(np.pi/180), goal_3*(np.pi/180), goal_4*(np.pi/180), goal_5*(np.pi/180)]
        return joint_goal # this list is input into the move.group.go function to get the robot to go to this coordinate


    # This function brings the robot back to its initial pose
    # Planned the robots movements in joint space, and the movement of the end effector
    # will be defined in this function
    def initial_pose(self):
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #only once
        
        #Initialization - by default the robot starts at this position
        joint_goal = self.jointarray(-166, -165, 77, -92, -194, 83)
        move_group.go(joint_goal, wait=True)
    
    
    # This function traces the capital letter R
    # Each Step defines one movement that creates part of the trace
    def trace_r(self):
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #only once
        
        #Step 1
        joint_goal = self.jointarray(-171, -146, 49, -83, -189, 89) # Each parameter is a coordinate of the respective joint in degrees
        move_group.go(joint_goal, wait=True) # Makes the robot move to the joint goal
       
        #Step 2
        joint_goal = self.jointarray(-165, -146, 65, -99, -195, 2)
        move_group.go(joint_goal, wait=True)
        
        #Step 3
        joint_goal = self.jointarray(-160, -161, 83, -102, -200, 2)
        move_group.go(joint_goal, wait=True)
        
        #Step 4
        joint_goal = self.jointarray(-167, -161, 66, -85, -193, 2)
        move_group.go(joint_goal, wait=True)
        
        #Step 5
        joint_goal = self.jointarray(-156, -173, 98, -105, -204, -37)
        move_group.go(joint_goal, wait=True)
        
        move_group.stop() # This is to ensure the robot stops completely and there is no residual movement
        
    
    # This function traces the capital letter I
    # Each Step defines one movement that creates part of the trace    
    def trace_i(self):
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #only once
        
        #Step 1
        joint_goal = self.jointarray(-156, -163, 95, -111, -204, 5)
        move_group.go(joint_goal, wait=True)
        
        #Step 2
        joint_goal = self.jointarray(-160, -165, 89, -103, -200, -0)
        move_group.go(joint_goal, wait=True)
        
        #Step 3
        joint_goal = self.jointarray(-167, -148, 65, -95, -193, 90)
        move_group.go(joint_goal, wait=True)
        
        #Step 4
        joint_goal = self.jointarray(-170, -148, 56, -85, -190, 176)
        move_group.go(joint_goal, wait=True)
        
        #Step 5
        joint_goal = self.jointarray(-164, -146, 70, -102, -196, 188)
        move_group.go(joint_goal, wait=True)
    
        move_group.stop() # This is to ensure the robot stops completely and there is no residual movement
        
        
    # This function traces the capital letter D  
    # Each Step defines one movement that creates part of the trace
    def trace_d(self):
        move_group = self.move_group
        joint_goal = move_group.get_current_joint_values() #only once
        
        #Step 1
        joint_goal = self.jointarray(-175, -149, 54, -85, -185, 89)
        move_group.go(joint_goal, wait=True)
        
        #Step 2
        joint_goal = self.jointarray(-145, -148, 89, -121, -215, -9)
        move_group.go(joint_goal, wait=True)
        
        #Step 3
        joint_goal = self.jointarray(-133, -161, 110, -129, -227, -88)
        move_group.go(joint_goal, wait=True)
        
        #Step 4
        joint_goal = self.jointarray(-134, -177, 114, -117, -226, -111)
        move_group.go(joint_goal, wait=True)
        
        #Step 5
        joint_goal = self.jointarray(-144, -182, 102, -100, -216, 2)
        move_group.go(joint_goal, wait=True)
        
        #Step 6
        joint_goal = self.jointarray(-154, -163, 78, -94, -206, 90)
        move_group.go(joint_goal, wait=True)
        
        move_group.stop() # This is to ensure the robot stops completely and there is no residual movement
        
        
# This is the main function, used to call the other functions
# It also creates user input, so that the letters can be traced out when the user allows it
def main():
    try: 
        print("")
        print("----------------------------------------------------------")
        print("Welcome to Riddhi's Initials")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to initialize position and begin the project ..."
        )
        initials = MidtermProject()
        initials.initial_pose() #Used to return to initial pose so that first letter can be drawn
        input("============ Press `Enter` to write the first letter, R ...")
        initials.trace_r() #function that traces R
        input("============ Press `Enter` to get ready for the next initial ...")
        initials.initial_pose() #Used to return to initial pose so that second letter can be drawn
        input("============ Press `Enter` to write the second letter, I ...")
        initials.trace_i() #function that traces I
        input("============ Press `Enter` to get ready for the last initial ...")
        initials.initial_pose() #Used to return to initial pose so that final letter can be drawn
        input("============ Press `Enter` to write the third letter, D ...")
        initials.trace_d() #function that traces D
        
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
        main()